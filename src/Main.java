import com.zuitt.example.Car;
import com.zuitt.example.Dog;

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
        myCar.drive();
        System.out.println(myCar.getDriverName());

        myCar.setBrand("Kia");
        myCar.setName("Sorrento");
        myCar.setYear_make(2022);
        System.out.println("The " + myCar.getBrand() + " " + myCar.getDriverName() + " " + myCar.getYear_make() + " was driven by " + myCar.getDriverName());

        Car yourCar = new Car();
        System.out.println("Your car was driven by: " + yourCar.getDriverName());

        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();
        System.out.println(myPet.getName() + " " + myPet.getColor() + " " + myPet.getBreed());
    }
}