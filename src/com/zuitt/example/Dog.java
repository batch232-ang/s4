package com.zuitt.example;

public class Dog extends Animal{
    private String breed;

    public Dog(){
        super();
        this.breed = "Golden Retriver";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    public String getBreed(){
        return this.breed;
    }

    public void setDogBreed(String dogBreed){
        this.breed = dogBreed;
    }

    public void speak(){
        super.call();
        System.out.println("Woof");
    }
}
