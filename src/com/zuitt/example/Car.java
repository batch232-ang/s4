package com.zuitt.example;

public class Car {
    private String name;
    private String brand;
    private int year_make;
    private Driver d;

    public Car(String name, String brand, int year_make){
        this.name = name;
        this.brand = brand;
        this.year_make = year_make;
        this.d = new Driver("Alejandro");
    }
    public Car(){
        this.d = new Driver("Alejandro");
    }
    public String getName(){
        return this.name;
    }
    public String getBrand(){
        return this.brand;
    }
    public int getYear_make(){
        return this.year_make;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }
    public void setYear_make(int year_make){
        this.year_make = year_make;
    }

    public void drive(){
        System.out.println("The car goes vroom vroom!");
    }
    public String getDriverName(){
        return this.d.getName();
    }

}
